from umqtt.simple import MQTTClient
import machine
import utime
import ntptime
import ujson
import time
from machine import Timer
from machine import Pin
import setup
from local_log import local_log

sensor = None
min_val = None
max_val = None
adc_pin = None

node_name = None
ssid = None
password = None
broker = None
period = None
sleep_time = None
wd_timeout = None
sleep_flag = None 
log = None
wdt = None

def sensor_init():
    global sensor
    global adc_pin
    print()
    
    sensor = machine.ADC(Pin(adc_pin))
    sensor.atten(machine.ADC.ATTN_11DB)
    
def mqtt_init():
    global client
    print("Initializing MQTT client.")
    print("node name:", node_name)
    client = MQTTClient(node_name, broker)

    client.connect()
            
def sensor_read_handle(timer):
    moist_read()
    wdt.feed() 
            
def moist_read():
    global sensor
    global client
    
    print("reading ADC")
    
    adc_value = sensor.read()
    print(adc_value)
    
    moist = (max_val-adc_value) / (max_val-min_val) * 100
    print("Soil moisture:", moist)
    
    try:
        client.publish(node_name + "/status", "online")
        client.publish(node_name + "/sens", "{ \"moist\": " + str(moist) + "}")
    except Exception as e:
        print("fail while publishing:", e)
        machine.reset()
        
def main ():
    global sensor 
    global min_val 
    global max_val 
    global adc_pin 
    global node_name 
    global ssid 
    global password 
    global broker 
    global period 
    global sleep_time 
    global wd_timeout 
    global sleep_flag 
    global log
    global wdt
    
    # read this from file
    json = setup.read_config("config.json")

    node_name = json["name"]
    ssid = json["ssid"]
    password = json["pass"]
    broker = json["broker"]
    adc_pin = json["adc_pin"]
    period = int(json["period"])
    wd_timeout = int(json["wd_timeout"])
    sleep_time = int(json["sleep_time"])
    sleep_flag = bool(json["sleep_flag"])
    min_val = int(json["min_adc_val"])
    max_val = int(json["max_adc_val"])

    log = local_log(node_name+".log")

    # initialize watchdog
    wdt = machine.WDT(timeout=wd_timeout*1000)

    print("starting the sensor")
    log.log_write("starting the sensor")

    # connect to network
    try:
        setup.network_connect(ssid, password)
    except Exception as e:
        log.log_write("Setting up network err:" + str(e))
    
    # Initialize DHT sensor
    sensor_init()
        
    # Initialize MQTT client and subscribe to rx_topic
    mqtt_init()
    
    try:
        if sleep_flag != True:
            print("NO SLEEP")
            tim = Timer(4)
            tim.init(mode=Timer.PERIODIC, period=period*1000, callback=sensor_read_handle)
            while True:
                pass
        else:
            moist_read()
            # wait for MQTT stack to send before sleep
            time.sleep(2)
 
            print("Reseting the watchdog and sleep for " + str(sleep_time) + "seconds")
            moist_read()
            # put the device to sleep for sleep_time
            machine.deepsleep(sleep_time*1000)
            
    except Exception as e:

        log.log_write("exception: " + str(e))
        machine.reset()

    log.log_write("WARNING: Main done")

if __name__ == '__main__':
    main()
