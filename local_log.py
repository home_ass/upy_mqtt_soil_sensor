import time
import os

class local_log:
    log_filename = ""
    log_file = None
    
    def __init__(self, name):
        timestamp = time.localtime()
        self.log_filename = name
        print("Log file: ", name)

    def log_write(self, data):
        if os.stat(self.log_filename)[6] > 1000:
            print("Removing "+ self.log_filename + " file")
            os.remove(self.log_filename)
        with open(self.log_filename, "a") as f:
            print("Appending to log", self.log_filename)
            timestamp = time.localtime()
            timestamp_str = str("%02d" % timestamp[3])+":"+str("%02d" % timestamp[4])+":"+str("%02d" % timestamp[5])
            f.write(timestamp_str +": "+ data+"\n")
            f.close()
