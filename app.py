from umqtt.simple import MQTTClient
import machine
import dht
import utime
import ntptime
import ujson
import time

from machine import Timer
from machine import Pin, ADC
import setup 

moist_sens = None
min_val = None
max_val = None
adc_pin = None

client = None
node_name = None
ssid = None
password = None
broker = None

period = None

def mqtt_init():
    global client
    print("Initializing MQTT client.")
    print("node name:", node_name)
    client = MQTTClient(node_name, broker)
    client.set_callback(mqtt_msg)

    client.connect()
  
def mqtt_msg(topic, msg):
    print("topic:", topic)
    print("message:", msg.decode("utf-8"))
    
    if topic.decode("utf-8") == (node_name + "/relay/set"):
        if msg.decode("utf-8") == 'OFF':
            print("LED OFF")
            client.publish(node_name + "/relay", "OFF")
            relay_pin.value(1)
        if msg.decode("utf-8") == 'ON':
            print("LED ON")
            client.publish(node_name + "/relay", "ON")
            relay_pin.value(0)

def moist_read(timer):
    global sensor
    global client
    
    adc_value = moist_sens.read()
    print(adc_value)
    
    moist = (max_val-adc_value) / (max_val-min_val) * 100
    
    try:
        client.publish(node_name + "/status", "online")
        client.publish(node_name + "/sens", "{ \"moist\": " + str(moist) + "}")
    except Exception as e:
        print("fail while publishing:", e)
        machine.reset()
        
# read this from file
json = setup.read_config("config.json")

node_name = json["name"]
ssid = json["ssid"]
password = json["pass"]
broker = json["broker"]
adc_pin = json["adc_pin"]
period = int(json["period"])
min_val = int(json["min_adc_val"])
max_val = int(json["max_adc_val"])


# Initialize sensor
print("Initializing DHT sensor.")
moist_sens = ADC(Pin(adc_pin))
moist_sens.atten(ADC.ATTN_11DB)

# Initialize MQTT client and subscribe to rx_topic
mqtt_init()

tim = Timer(4)
tim.init(mode=Timer.PERIODIC, period=period*1000, callback=moist_read)

